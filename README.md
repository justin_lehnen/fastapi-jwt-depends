# FastAPI-JWT-Depends

## Usage

### Example

```py
from typing import Annotated, Union
from fastapi import FastAPI, APIRouter, Request, Depends, HTTPException, status
from fastapi.responses import JSONResponse

from fastapi_jwt_depends import create_access_token, create_refresh_token, algorithms
from fastapi_jwt_depends import JWTRequired, JWTOptional, FreshJWTRequired, RefreshJWTRequired
from fastapi_jwt_depends import AccessToken, AccessTokenData, RefreshToken, RefreshTokenData
from fastapi_jwt_depends import JWTError, ExpiredSignatureError


secret_key = 'This should be very secret'
auth_algorithms = [algorithms.HS512]


app = FastAPI()
fake_db = {
    'revoked-tokens': {},
    'users': {
        '01HBNFDNCVRH3EFGDA66T4QVQN': {
            'name': 'Harry',
        },
        '01HBNFE2VWB57G6YGW51ET7SYF': {
            'name': 'John',
        },
        '01HBNFE4RPJMKJDPX5C5FAA29Y': {
            'name': 'Emily',
        },
    }
}


@app.exception_handler(ExpiredSignatureError)
def expired_token_exception_handler(request: Request, exc: ExpiredSignatureError):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={'detail': str(exc)},
    )


@app.exception_handler(JWTError)
def expired_token_exception_handler(request: Request, exc: JWTError):
    return JSONResponse(
        status_code=status.HTTP_403_FORBIDDEN,
        content={'detail': str(exc)},
    )


def is_denied(payload: dict[str, ...]) -> bool:
    return payload.get('jti') in fake_db['revoked-tokens']


auth_optional = JWTOptional(secret_key, auth_algorithms, deny_check=is_denied)
auth = JWTRequired(secret_key, auth_algorithms, deny_check=is_denied)
auth_fresh = FreshJWTRequired(secret_key, auth_algorithms, deny_check=is_denied)
auth_refresh = RefreshJWTRequired(secret_key, auth_algorithms, deny_check=is_denied)


auth_router = APIRouter(prefix='/auth', tags=['auth'])


@auth_router.get('/token', tags=['auth'])
async def create_jwt(user_id: str) -> RefreshToken:
    if user_id in fake_db['users']:
        access = create_access_token(secret_key, auth_algorithms[0], subject=user_id)
        refresh = create_refresh_token(secret_key, auth_algorithms[0], subject=user_id)
        return RefreshToken(access_token=access, refresh_token=refresh)

    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Login failed')


@auth_router.delete('/token', tags=['auth'])
async def revoke_jwt(token_data: Annotated[AccessTokenData, Depends(auth)]) -> str:
    fake_db['revoked-tokens'][str(token_data.jti)] = {}
    return 'ok'


@auth_router.get('/refresh', tags=['auth'])
async def refresh_jwt(token_data: Annotated[RefreshTokenData, Depends(auth_refresh)]) -> AccessToken:
    access = create_access_token(secret_key, auth_algorithms[0], subject=token_data.sub)
    return AccessToken(access_token=access)


@auth_router.delete('/refresh', tags=['auth'])
async def revoke_refresh_jwt(token_data: Annotated[RefreshTokenData, Depends(auth_refresh)]) -> str:
    fake_db['revoked-tokens'][str(token_data.jti)] = {}
    return 'ok'


@auth_router.get('/fresh', tags=['auth'])
async def create_fresh_jwt(user_id: str) -> AccessToken:
    if user_id in fake_db['users']:
        access = create_access_token(secret_key, auth_algorithms[0], subject=user_id, fresh=True)
        return AccessToken(access_token=access)

    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Login failed')


@app.get('/profile')
async def read_profile(token_data: Annotated[Union[AccessTokenData, None], Depends(auth_optional)]) -> str:
    return fake_db['users'].get(token_data.sub).get('name') if token_data else 'Guest'


@app.get('/profile/count')
async def read_profile(token_data: Annotated[Union[AccessTokenData, None], Depends(auth)]) -> int:
    return len(fake_db['users'])


@app.delete('/profile')
async def remove_profile(token_data: Annotated[AccessTokenData, Depends(auth_fresh)]) -> dict:
    return fake_db['users'].pop(token_data.sub)


app.include_router(auth_router)
```
