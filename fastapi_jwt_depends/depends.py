from fastapi import Security
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jose import JWTError  # noqa

import typing

from .models import AccessTokenData, RefreshTokenData
from .utils import decode_token


bearer = HTTPBearer(scheme_name='Access Token')
bearer_refresh = HTTPBearer(scheme_name='Refresh Token')
bearer_optional = HTTPBearer(scheme_name='Access Token', auto_error=False)


class JWTDependency(object):
    def __init__(
        self,
        secret_key: str,
        algorithms: list[str],
    ):
        super(JWTDependency, self).__init__()
        self.secret_key = secret_key
        self.algorithms = algorithms


class JWTRequired(JWTDependency):
    def __init__(
        self,
        secret_key: str,
        algorithms: list[str],
        deny_check: typing.Callable[[dict[str, typing.Any]], bool] | None = None,
        leeway: int = 60,
    ):
        super(JWTRequired, self).__init__(secret_key, algorithms)
        self.deny_check = deny_check
        self.leeway = leeway

    def __call__(
        self,
        authorization_credentials: typing.Annotated[HTTPAuthorizationCredentials, Security(bearer)],
    ) -> AccessTokenData:
        token_data_dict = decode_token(
            authorization_credentials.credentials,
            self.secret_key,
            algorithms=self.algorithms,
            deny_check=self.deny_check,
            leeway=self.leeway,
        )

        if token_data_dict.get('type') != 'access':
            raise JWTError('Not an access token.')

        return AccessTokenData.model_validate(token_data_dict)


class JWTOptional(JWTRequired):
    def __init__(
        self,
        secret_key: str,
        algorithms: list[str],
        deny_check: typing.Callable[[dict[str, typing.Any]], bool] | None = None,
        leeway: int = 60,
    ):
        super(JWTOptional, self).__init__(secret_key, algorithms, deny_check, leeway)

    def __call__(
        self,
        authorization_credentials: typing.Annotated[HTTPAuthorizationCredentials, Security(bearer_optional)],
    ) -> AccessTokenData | None:
        if authorization_credentials is None:
            return

        return super(JWTOptional, self).__call__(authorization_credentials)


class FreshJWTRequired(JWTRequired):
    def __init__(
        self,
        secret_key: str,
        algorithms: list[str],
        deny_check: typing.Callable[[dict[str, typing.Any]], bool] | None = None,
        leeway: int = 60,
    ):
        super(FreshJWTRequired, self).__init__(secret_key, algorithms, deny_check, leeway)

    def __call__(
        self,
        authorization_credentials: typing.Annotated[HTTPAuthorizationCredentials, Security(bearer)],
    ) -> AccessTokenData:
        token_data = super(FreshJWTRequired, self).__call__(authorization_credentials)

        if not token_data.fresh:
            raise JWTError('Access token is not fresh.')

        return token_data


class RefreshJWTRequired(JWTRequired):
    def __init__(
        self,
        secret_key: str,
        algorithms: list[str],
        deny_check: typing.Callable[[dict[str, typing.Any]], bool] | None = None,
        leeway: int = 60,
    ):
        super(RefreshJWTRequired, self).__init__(secret_key, algorithms, deny_check, leeway)

    def __call__(
        self,
        authorization_credentials: typing.Annotated[HTTPAuthorizationCredentials, Security(bearer_refresh)],
    ) -> RefreshTokenData:
        token_data_dict = decode_token(
            authorization_credentials.credentials,
            self.secret_key,
            algorithms=self.algorithms,
            deny_check=self.deny_check,
            leeway=self.leeway,
        )

        if token_data_dict.get('type') != 'refresh':
            raise JWTError('Not a refresh token.')

        return RefreshTokenData.model_validate(token_data_dict)
