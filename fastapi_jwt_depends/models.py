from pydantic import BaseModel, ConfigDict, Field
from pydantic import UUID4
from pydantic.alias_generators import to_camel

import typing
import datetime


class TokenData(BaseModel):
    model_config = ConfigDict(
        populate_by_name=True,
        alias_generator=to_camel,
        arbitrary_types_allowed=True,
    )

    jti: UUID4
    iat: datetime.datetime
    nbf: datetime.datetime
    exp: datetime.datetime
    sub: str = None
    data: dict = None


class AccessTokenData(TokenData):
    type_: typing.Literal['access'] = Field('access', alias='type')
    fresh: bool = False


class RefreshTokenData(TokenData):
    type_: typing.Literal['refresh'] = Field('refresh', alias='type')


class Token(BaseModel):
    model_config = ConfigDict(
        populate_by_name=True,
        alias_generator=to_camel,
    )

    token_type: str = Field('bearer')
    access_token: str


class AccessToken(Token):
    pass


class RefreshToken(Token):
    refresh_token: str
