from jose import JWTError, jwt  # noqa

import typing
import datetime
import uuid


def create_token(
    secret_key: str,
    algorithm: str,
    data: dict | None = None,
    subject: str = None,
    expire_delta: datetime.timedelta | int | None = None,
    fresh: bool | None = None,
    type_: str | None = None,
) -> str:
    data = data or {}
    expire_delta = expire_delta or datetime.timedelta(minutes=15)
    ts = datetime.datetime.now(datetime.timezone.utc)
    jti = str(uuid.uuid4())

    if isinstance(expire_delta, datetime.timedelta):
        expire_delta = expire_delta.total_seconds()

    expire = int(datetime.datetime.now(datetime.timezone.utc).timestamp()) + expire_delta

    to_encode = {}
    to_encode.update({
        'jti': jti,  # JWT ID
        'iat': ts,  # Issued at
        'nbf': ts,  # Not valid before
        'exp': expire,  # Expiration time
        'type': type_ or 'access',
    })

    if subject is not None:
        to_encode['sub'] = subject

    if fresh is not None:
        to_encode['fresh'] = fresh

    to_encode['data'] = data.copy()

    return jwt.encode(to_encode, secret_key, algorithm=algorithm)


def create_access_token(
    secret_key: str,
    algorithm: str,
    data: dict | None = None,
    subject: str = None,
    expire_delta: datetime.timedelta | int | None = None,
    fresh: bool = False,
) -> str:
    return create_token(
        secret_key,
        algorithm,
        data,
        subject,
        expire_delta,
        fresh,
        type_='access'
    )


def create_refresh_token(
    secret_key: str,
    algorithm: str,
    data: dict | None = None,
    subject: str = None,
    expire_delta: datetime.timedelta | int | None = None,
) -> str:
    return create_token(
        secret_key,
        algorithm,
        data,
        subject,
        expire_delta,
        None,
        type_='refresh'
    )


def get_token(
    headers: dict[str, str],
    *,
    name: str = 'Authorization',
    type_: str = 'Bearer',
) -> str | None:
    if name not in headers:
        return

    parts = headers.get(name).split()

    if not len(parts):
        raise JWTError('Header has no value.')

    if len(parts) == 2 and type_ is not None:
        # <HeaderName>: <HeaderType> <JWT>
        bearer, token = parts
        allowed_bearer_type = type_.lower()

        if bearer.lower() != allowed_bearer_type:
            raise JWTError(f'Only {allowed_bearer_type} types are allowed.')

        return token

    if len(parts) == 1 and type_ is None:
        # <HeaderName>: <JWT>
        return parts[0]

    raise JWTError('Invalid header format.')


def decode_token(
    token: str,
    secret_key: str,
    *,
    algorithms: list[str],
    deny_check: typing.Callable[[dict[str, typing.Any]], bool] | None = None,
    leeway: int = 60
) -> dict[str, typing.Any]:
    payload = jwt.decode(
        token=token,
        key=secret_key,
        algorithms=algorithms,
        options={
            'leeway': leeway,
            'require_iat': True,
            'require_nbf': True,
            'require_jti': True,
            'require_exp': True,
        },
    )

    if callable(deny_check) and deny_check(payload):
        raise JWTError('JWT was denied access.')

    if payload.get('type') not in ('access', 'refresh'):
        raise JWTError('Unknown JWT type.')

    return payload
